from unittest.mock import Mock
import unittest
from vector2d import Vector2D
from main import Rotate


class RotatingTests(unittest.TestCase):
    def test_set_direction(self):
        rotable_mock = Mock()
        rotable_mock.get_direction.return_value = 2
        rotable_mock.get_angular_velocity.return_value = -4
        rotable_mock.get_directions_number.return_value = 8

        cmd = Rotate(rotable_mock)
        cmd.execute()

        rotable_mock.set_direction.assert_called_with(6)

    def test_try_get_direction_exeption_exeption(self):
        """ try to read direction when unable to read """
        rotable_mock = Mock()
        rotable_mock.get_direction.side_effect = Exception
        rotable_mock.get_angular_velocity.return_value = -4
        rotable_mock.get_directions_number.return_value = 8

        cmd = Rotate(rotable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_try_get_angular_velocity_exeption(self):
        """ try to read angular velocity when unable to read """
        rotable_mock = Mock()
        rotable_mock.get_direction.return_value = 2
        rotable_mock.get_angular_velocity.side_effect = Exception
        rotable_mock.get_directions_number.return_value = 8

        cmd = Rotate(rotable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_try_get_directions_number_exeption(self):
        """ try to read directions number when unable to read """
        rotable_mock = Mock()
        rotable_mock.get_direction.return_value = 2
        rotable_mock.get_angular_velocity.return_value = -4
        rotable_mock.get_directions_number.side_effect = Exception

        cmd = Rotate(rotable_mock)

        with self.assertRaises(Exception):
            cmd.execute()

    def test_try_set_direction_exeption(self):
        """ try to write direction  when unable to wtire """
        rotable_mock = Mock()
        rotable_mock.get_direction.return_value = 2
        rotable_mock.get_angular_velocity.return_value = -4
        rotable_mock.get_directions_number.return_value = 8

        rotable_mock.set_direction.side_effect = Exception

        cmd = Rotate(rotable_mock)

        with self.assertRaises(Exception):
            cmd.execute()