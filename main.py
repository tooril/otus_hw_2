""" Moving and Ratation """
from vector2d import Vector2D


class Movable:
    """ Interface of moving object """
    def set_position(self, vector: Vector2D):
        """ set position of object """

    def get_position(self) -> Vector2D:
        """ return position of object """

    def get_velocity(self) -> Vector2D:
        """ return position of object """


class Move:
    """ Execute a move of object """
    def __init__(self, obj: Movable):
        self.obj = obj

    def execute(self):
        """ execute a moving """
        self.obj.set_position(self.obj.get_position() +
                              self.obj.get_velocity())


class Rotable:
    """ Interface of rotating object """
    def set_direction(self, value: int):
        """ set direction of object """

    def get_direction(self) -> int:
        """ return direction of object """

    def get_directions_number(self) -> int:
        """ return directions number """

    def get_angular_velocity(self) -> int:
        """ return angular velocity of object """


class Rotate:
    """ Execute a rotation of object """
    def __init__(self, obj: Rotable):
        self.obj = obj

    def execute(self):
        """ execute a rotating """
        self.obj.set_direction((self.obj.get_direction()
                               + self.obj.get_angular_velocity())
                               % self.obj.get_directions_number())
